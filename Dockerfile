FROM docker.zandura.net/docker/debian-jessie

## install basic packages and prepare web-root
RUN set -xe \
    && curl -Lso /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg \
    && echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list \
    && { \
        echo "Package: *"; \
        echo "Pin: origin packages.sury.org"; \
        echo "Pin-Priority: 1000"; \
    } > /etc/apt/preferences.d/php-survy \
    && apt-get -qq update \
    && DEBIAN_FRONTEND=noninteractive apt-get -qqy --no-install-recommends dist-upgrade > /dev/null \
    && DEBIAN_FRONTEND=noninteractive apt-get -qqy --no-install-recommends install \
        php7.1 \
        php7.1-cli \
    > /dev/null \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD ["/usr/bin/php","-a"]

